/**
 * Main application controller
 **/
;(function() {


  angular
    .module('hrn')
    .controller('MainController', ['$timeout', '$scope', '$rootScope', function ($timeout, $scope, $rootScope) {
        var loaderDelay = 1200;

        // Detect current page
        $rootScope.$on('$routeChangeSuccess', function(ev,data) {
            $rootScope.location = (data.$$route.originalPath != null) ? data.$$route.originalPath : '/';
        });

        // Loader init
        angular.element(document).ready(function () {
            $timeout( function(){
                $('.loader').addClass('loaded');
            }, loaderDelay );
        });

        // Ticket Open-Close Function
        $scope.ticketsOpened = false;

        $scope.showBenefits = function () {
            if ($('.ticket').hasClass('open')) {
                $('.ticket').removeClass('open');
                $scope.ticketsOpened = false;
            } else {
                $('.ticket').addClass('open');
                $scope.ticketsOpened = true;
            }
        };

        // Menu Open-Close Function
        $scope.openMenu = function () {
            if ($('.header__toggler').hasClass('open')) {
                $('.header__toggler').removeClass('open');
                $('.main-nav').removeClass('open');
            } else {
                $('.header__toggler').addClass('open');
                $('.main-nav').addClass('open');
            }
        };

        // Toggle Class Function (Parent)
        $scope.toggleClassParent = function (event){
            $(event.target).parent().siblings().removeClass('active');
            $(event.target).parent().addClass('active');
        };
    }]);

  // Scrolling on click Function
  angular.module('hrn')
      .directive('scrollOnClick', function() {
          return {
              restrict: 'EA',
              link: function(scope, $elm) {

                  $elm.on('click', function() {
                      $("html,body").animate({scrollTop: $('.ticket-wrapper').position().top + 'px'}, "slow");
                  });
              }
          }
      });

})();