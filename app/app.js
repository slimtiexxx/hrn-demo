(function() {


  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('hrn', [
      'ngRoute',
      'alexjoffroy.angular-loaders'
    ])
    .config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   * 
   */
  function config($routeProvider, $locationProvider, $httpProvider) {

    $locationProvider.html5Mode(false);

    // routes
    $routeProvider
      .when('/', {
        templateUrl: 'views/single-attandee.html',
        controller: 'MainController'
      })
      .when('/group-tickets', {
        templateUrl: 'views/group-tickets.html',
        controller: 'MainController'
      })
      .when('/investors', {
        templateUrl: 'views/investors.html',
        controller: 'MainController'
      })
      .when('/startups', {
        templateUrl: 'views/startups.html',
        controller: 'MainController'
      })
      .otherwise({
        redirectTo: '/'
      });

    $httpProvider.interceptors.push('authInterceptor');

  }


  /**
   * You can intercept any request or response inside authInterceptor
   * or handle what should happend on 40x, 50x errors
   * 
   */
  angular
    .module('hrn')
    .factory('authInterceptor', ['$rootScope', '$q', 'LocalStorage', '$location', function ($rootScope, $q, LocalStorage, $location) {
        return {

            // intercept every request
            request: function(config) {
                config.headers = config.headers || {};
                return config;
            },

            // Catch 404 errors
            responseError: function(response) {
                if (response.status === 404) {
                    $location.path('/');
                    return $q.reject(response);
                } else {
                    return $q.reject(response);
                }
            }
        };
    }]);


})();